#include "pathfinder.h"

static void init(t_dij *t, t_info *info) {
    t->dist = (int *)malloc(sizeof(int) * info->islandsnum);
    t->visited = (bool *)malloc(sizeof(bool) * info->islandsnum);
    t->parent = (t_list **)malloc(sizeof(t_list *) * info->islandsnum);
    t->routes = NULL;
    t->islandsnum = info->islandsnum;
    for (int i = 0; i < info->islandsnum; ++i) {
        t->dist[i] = INF;
        t->visited[i] = false;
        t->parent[i] = NULL;
    }
}

static void update_dist_parent(t_info *info, t_dij *t, int v, int u) {
    if (t->dist[u] + info->cost_matrix[u][v] < t->dist[v]) {
        mx_clear_list(&t->parent[v]);
        t->dist[v] = t->dist[u] + info->cost_matrix[u][v];
    }
    mx_push_front(&t->parent[v], mx_return_int(u));
}

static void dijkstra_loop(t_info *info, t_dij *t) {
    int u;

    for (int i = 0; i < info->islandsnum - 1; ++i) {
        u = mx_min_distance(t);
        t->visited[u] = true;
        for (int v = 0; v < info->islandsnum; ++v)
            if (!t->visited[v] && info->cost_matrix[u][v] && t->dist[u] != INF
             && t->dist[u] + info->cost_matrix[u][v] <= t->dist[v])
                update_dist_parent(info, t, v, u);
    }
}

static int cmp(void *lst1, void *lst2) {
    if (*(int *)mx_get_back_list(lst1) < *(int *)mx_get_back_list(lst2))
        return -1;
    if (*(int *)mx_get_back_list(lst1) == *(int *)mx_get_back_list(lst2)) {
        int end = mx_list_size(lst1) > mx_list_size(lst2) ?
                  mx_list_size(lst2) : mx_list_size(lst1);

        for (int i = 1; i < end; ++i) {
            t_list *tmp1 = mx_get_element_by_index(lst1, i);
            t_list *tmp2 = mx_get_element_by_index(lst2, i);

            if (*(int *)tmp1->data != *(int *)tmp2->data)
                return *(int *)tmp1->data - *(int *)tmp2->data;
        }
    }
    return 1;
}

void mx_dijkstra(t_info *info) {
    t_dij t;

    for (t_ll i = 0; i < info->islandsnum - 1; ++i) {
        init(&t, info);
        t.dist[i] = 0;
        t.src = i;
        dijkstra_loop(info, &t);
        mx_create_routes(&t);
        mx_sort_list(t.routes, cmp);
        mx_print_routes(&t, info);
        mx_free_dijksrta_var(&t, info);
    }
}
