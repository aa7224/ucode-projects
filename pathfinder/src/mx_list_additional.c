#include "pathfinder.h"

void *mx_get_back_list(t_list *list) {
    t_list *temp = NULL;

    for (temp = list; temp->next; temp = temp->next);
    return temp->data;
}

t_list *mx_get_element_by_index(t_list *list, int index) {
    t_list *temp = NULL;

    if (list)
        for (temp = list; index--; temp = temp->next);
    return temp;
}


t_list *mx_list_cpy(t_list *src) {
    t_list *dst = NULL;

    for (t_list *temp = src; temp; temp = temp->next)
        mx_push_back(&dst, mx_return_int(*(int *)temp->data));
    return dst;
}

int *mx_return_int(int num) {
    int *ret = (int *)malloc(sizeof(int));

    *ret = num;
    return ret;
}
