#include "pathfinder.h"

char *mx_get_first_island(char *str) {
    return mx_strndup(str, mx_get_char_index(str, '-'));
}

char *mx_get_second_island(char *str) {
    return mx_strndup(str + mx_get_char_index(str, '-') + 1,
                      mx_get_char_index(str, ',')
                      - mx_get_char_index(str, '-') - 1);
}

int mx_get_distance(char *str) {
    return mx_atoi(str + mx_get_char_index(str, ',') + 1);
}
