#include "pathfinder.h"

static void printerr_char(const char c) {
    write(2, &c, 1);
}

static void printnum(int num) {
    if(num > 9)
        printnum(num / 10);
    printerr_char(num % 10 + 48);
}

void mx_printinterr(int n) {
    if (n == -2147483648) {
        write(2, "-2147483648", 11);
        return;
    }
    if(n < 0) {
        mx_printerr("-");
        n *= -1;
    }
    printnum(n);
}
