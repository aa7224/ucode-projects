#include "pathfinder.h"

static bool error_print() {
    mx_printerr("error: invalid number of islands\n");
    return 0;
}

int mx_is_valid_islandsnum(t_info *info, char *file) {
    int fd = open(file, O_RDONLY);
    char *str = NULL;
    int f_line = 0;
    t_ll i = 0;

    mx_read_line(&str, 1, '\n', fd);
    f_line = mx_atoi(str);
    for (i = 0; info->islands[i]; ++i);
    if (info->is_f_line_valid && i == info->islandsnum) {
        mx_strdel(&str);
        close(fd);
        return true;
    }
    error_print();
    mx_strdel(&str);
    close(fd);
    return false;
}
