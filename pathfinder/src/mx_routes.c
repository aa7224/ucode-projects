#include "pathfinder.h"

static void add_route(t_dij *t, t_list *route) {
    t_list *temp = NULL;
    int size = mx_list_size(t->parent[*(int *)route->data]);

    for (int i = 0; i < size; ++i) {
        temp = t->parent[*(int *)route->data];
        temp = mx_get_element_by_index(temp, i);


        mx_push_front(&route, mx_return_int(*(int *)temp->data));

        if (t->src != *(int *)route->data) {
            add_route(t, route);
        }
        else
            mx_push_back(&t->routes, mx_list_cpy(route));
        mx_pop_front(&route);
    }
}

static inline void print_route(t_dij *t, t_info *info, t_list *route) {
    t_list *temp = NULL;
    int size = mx_list_size(route);

    for (t_ll i = 0; i < size - 1 && t; ++i) {
        temp = mx_get_element_by_index(route, i);
        mx_printstr(info->islands[*(int *)temp->data]->name);
        mx_printstr(" -> ");
    }
    temp = mx_get_element_by_index(route, size - 1);
    mx_printstr(info->islands[*(int *)temp->data]->name);
    mx_printstr("\n");
}

static inline void print_distance(t_dij *t, t_list *route) {
    t_list *node = NULL;
    int size = mx_list_size(route);
    int dist = 0;

    for (int i = 1; i < size - 1; ++i) {
        node = mx_get_element_by_index(route, i);
        mx_printint(t->dist[*(int *)node->data] - dist);
        mx_printstr(" + ");
        dist = t->dist[*(int *)node->data];
    }
    mx_printint(t->dist[*(int *)mx_get_back_list(route)] - dist);
    mx_printstr(" = ");
    mx_printint(t->dist[*(int *)mx_get_back_list(route)]);
}

void mx_print_routes(t_dij *t, t_info *info) {
    for (t_list *temp = t->routes; temp; temp = temp->next) {
        mx_printstr("========================================\nPath: ");
        mx_printstr(info->islands[t->src]->name);
        mx_printstr(" -> ");
        mx_printstr(info->islands[*(int *)mx_get_back_list(temp->data)]->name);
        mx_printstr("\nRoute: ");
        print_route(t, info, temp->data);
        mx_printstr("Distance: ");
        if (mx_list_size(temp->data) > 2)
            print_distance(t, temp->data);
        else
            mx_printint(t->dist[*(int *)mx_get_back_list(temp->data)]);
        mx_printstr("\n========================================\n");
    }
}

void mx_create_routes(t_dij *t) {
    t_list *route = NULL;

    for (t_ll i = t->src + 1; i < t->islandsnum; ++i) {
        mx_push_front(&route, mx_return_int(i));
        add_route(t, route);
        mx_pop_front(&route);
    }
}
