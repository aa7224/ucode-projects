#include "pathfinder.h"

int **mx_create_cost_matrix(t_info *info) {
    int **cost_matrix = (int **)malloc(sizeof(int *) * info->islandsnum);
    t_ll i;

    for (i = 0; i < info->islandsnum; i++) {
        cost_matrix[i] = (int *)malloc(sizeof(int) * info->islandsnum);
        for (t_ll j = 0; j < info->islandsnum; j++)
            cost_matrix[i][j] = 0;
    }
    for (i = 0; info->bridge_list[i]; i++) {
        cost_matrix[info->bridge_list[i]->id_from][info->bridge_list[i]->id_to]
         = info->bridge_list[i]->distance;
        cost_matrix[info->bridge_list[i]->id_to][info->bridge_list[i]->id_from]
         = info->bridge_list[i]->distance;
    }
    return cost_matrix;
}
