#include "pathfinder.h"

static bool is_island_exist(t_info *info, char *island) {
    for (int i = 0; info->islands[i]; ++i)
        if (mx_strcmp(info->islands[i]->name, island) == 0)
            return true;
    return false;
}

static bool is_bridge_exist(t_info *info, char *line) {
    char *f_island = mx_get_first_island(line);
    char *s_island = mx_get_second_island(line);

    for (int i = 0; info->bridge_list[i]; ++i) {
        if (mx_strcmp(info->bridge_list[i]->first, f_island) == 0
            || mx_strcmp(info->bridge_list[i]->second, f_island) == 0)
            if (mx_strcmp(info->bridge_list[i]->first, s_island) == 0
                || mx_strcmp(info->bridge_list[i]->second, s_island) == 0) {
                mx_strdel(&f_island);
                mx_strdel(&s_island);
                return true;
            }
    }
    mx_strdel(&f_island);
    mx_strdel(&s_island);
    return false;
}

static t_bridge *create_bridge(char *first, char *second, int distance) {
    t_bridge *new_bridge = (t_bridge *)malloc(sizeof(t_bridge));

    if (new_bridge) {
        new_bridge->first = first;
        new_bridge->second = second;
        new_bridge->distance = distance;
        new_bridge->id_from = 0;
        new_bridge->id_to = 0;
    }
    return new_bridge;
}

void mx_push_island(t_info *info, char *island, bool *flag) {
    t_ll i = 0;

    if (!is_island_exist(info, island)) {
        for (i = 0; info->islands[i]; ++i);
        if (i >= info->islandsnum) {
            info->is_f_line_valid = false;
            mx_strdel(&island);
            *flag = false;
            return;
        }
        info->islands[i] = mx_create_island(i, island);
    }
    else 
        mx_strdel(&island);
}

void mx_push_bridge(t_info *info, char *line, int linenum, void (error)(int)) {
    t_ll i = 0;

    if (!is_bridge_exist(info, line)) {
        for (i = 0; info->bridge_list[i]; ++i);
        info->bridge_list[i] = create_bridge(mx_get_first_island(line) ,
                                             mx_get_second_island(line),
                                             mx_get_distance(line));
    }
    else {
        error(linenum);
        mx_free_islands(info->islands);
        mx_free_bridges(info->bridge_list);
        exit(0);
    }
}
