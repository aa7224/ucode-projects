#include "pathfinder.h"

static bool is_bridge_exist(t_island *island, t_bridge *bridge) {
    if (mx_strcmp(island->name, bridge->first) == 0)
        return true;
    return false;
}

static bool is_bridge_exist_second(t_island *island, t_bridge *bridge) {
    if (mx_strcmp(island->name, bridge->second) == 0)
        return true;
    return false;
}

void mx_give_ids(t_info *info) {
    for (t_ll i = 0; info->bridge_list[i]; i++) {
        for (t_ll j = 0; info->islands[j]; j++) {
            if (is_bridge_exist(info->islands[j], info->bridge_list[i])) {
                info->bridge_list[i]->id_from = info->islands[j]->id;
            }
            else if (is_bridge_exist_second(info->islands[j],
                                            info->bridge_list[i])) {
                info->bridge_list[i]->id_to = info->islands[j]->id;
            }
        }
    }
}
