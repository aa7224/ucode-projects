#include "pathfinder.h"

void mx_del_cost_matrix(int **cost_matrix, int size) {
    if (cost_matrix) {
        for (int i = 0; i < size; i++)
            free(cost_matrix[i]);
        free(cost_matrix);
    }
}

void mx_free_bridges(t_bridge **bridges) {
    for (int i = 0; bridges[i]; i++) {
        free(bridges[i]->first);
        free(bridges[i]->second);
        free(bridges[i]);
    }
    free(bridges);
}

void mx_free_islands(t_island **islands) {
    for (int i = 0; islands[i]; i++) {
        free(islands[i]->name);
        free(islands[i]);
    }
    free(islands);
}

void mx_free_dijksrta_var(t_dij *t, t_info *info) {
    free(t->dist);
    free(t->visited);
    for (t_list *temp = t->routes; temp; temp = temp->next) {
        t_list *to_clear = temp->data;

        mx_clear_list(&to_clear);
    }
    for (t_ll i = 0; i < info->islandsnum; i++) {
        mx_clear_list(&t->parent[i]);
    }
    free(t->parent);
    mx_clear_list(&t->routes);
}
