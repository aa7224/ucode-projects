#include "pathfinder.h"

bool mx_is_valid_argc(int argc) {
    if (argc != 2) {
        mx_printerr("usage: ./pathfinder [filename]\n");
        return false;
    }
    return true;
}

bool mx_is_valid_file(char *file) {
    int fd = 0;

    if ((fd = open(file, O_RDONLY)) == -1) {
        mx_printerr("error: file ");
        mx_printerr(file ? file : "");
        mx_printerr(" does not exist\n");
        return false;
    }
    close(fd);
    return true;
}

bool mx_is_file_empty(char *file) {
    int fd = open(file, O_RDONLY);
    char buffer;

    if (!read(fd, &buffer, 1)) {
        mx_printerr("error: file ");
        mx_printerr(file);
        mx_printerr(" is empty\n");
        close(fd);
        return false;
    }
    close(fd);
    return true;
}

static bool check_len(char *line) {
    unsigned long long islandsnum = 0;

    islandsnum = mx_atoi(line);
    if (mx_strlen(line) == 0) {
        mx_printerr("error: line 1 is not valid\n");
        return false;
    }
    return true;
}

bool mx_is_valid_first_line(t_info *info, char *file) {
    int fd = open(file, O_RDONLY);
    char *line = NULL;

    mx_read_line(&line, 1, '\n', fd);
    if (!check_len(line)) {
        close(fd);
        mx_strdel(&line);
        return false;
    }
    for (int i = 0; line[i]; i++)
        if (!mx_isdigit(line[i])) {
            mx_printerr("error: line 1 is not valid\n");
            close(fd);
            mx_strdel(&line);
            return false;
        }
    info->islandsnum = mx_atoi(line);
    mx_strdel(&line);
    close(fd);
    return true;
}
