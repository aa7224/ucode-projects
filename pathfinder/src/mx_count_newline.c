#include "pathfinder.h"

int mx_count_enters(char *file) {
    char *str = mx_file_to_str(file);
    int count = 0;
    
    for (int i = 0; str[i]; i++)
        if (str[i] == '\n')
            count++;
    mx_strdel(&str);
    return count;
}
