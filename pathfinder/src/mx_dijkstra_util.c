#include "pathfinder.h"

int mx_min_distance(t_dij *t) {
    int min = INF;
    int min_index = 0;

    for (t_ll v = 0; v < t->islandsnum; v++) 
        if (t->visited[v] == false && t->dist[v] <= min) {
            min = t->dist[v];
            min_index = v;
        }
    return min_index; 
}
