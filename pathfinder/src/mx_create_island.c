#include "pathfinder.h"

t_island *mx_create_island(int id, char *name) {
    t_island *new_island = (t_island *)malloc(sizeof(t_island));

    if (new_island) {
        new_island->id = id;
        new_island->name = name;
    }
    return new_island;
}
