#include "pathfinder.h"

static bool is_valid_islands(char *f_island, char *s_island) {
    if (mx_strcmp(f_island, s_island) == 0) {
        mx_strdel(&f_island);
        mx_strdel(&s_island);
        return false;
    }
    for (int i = 0; f_island[i]; ++i)
        if (!mx_isalpha(f_island[i])) {
            mx_strdel(&f_island);
            mx_strdel(&s_island);
            return false;
        }
    for (int i = 0; s_island[i]; ++i)
        if (!mx_isalpha(s_island[i])) {
            mx_strdel(&f_island);
            mx_strdel(&s_island);
            return false;
    }
    mx_strdel(&f_island);
    mx_strdel(&s_island);
    return true;
}

static bool is_valid_distance(char *dist) {
    int dst = mx_atoi(dist);

    for (int i = mx_get_char_index(dist, ',') + 1; dist[i]; ++i)
        if (!mx_isdigit(dist[i]))
            return false;
    if (dst >= INF && dst <= 0)
        return false;
    return true;
}

static bool is_valid_line_format(char *line) {
    int d_1 = 0;
    int d_2 = 0;

    if (mx_strlen(line) == 0)
        return false;
    for (int i = 0; line[i]; ++i) {
        if (line[i] == '-')
            ++d_1;
        if (line[i] == ',')
            ++d_2;
    }
    if (d_1 == 1 && d_2 == 1)
        return true;
    return false;
}

bool mx_is_line_valid(char *line) {
    if (is_valid_line_format(line)) {
        if (is_valid_islands(mx_get_first_island(line)
                             , mx_get_second_island(line)))
            if (is_valid_distance(line))
                return true;
    }
    return false;
}
