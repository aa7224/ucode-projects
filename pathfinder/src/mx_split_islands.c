#include "pathfinder.h"

static void wrong_format(int linenum) {
    mx_printerr("error: line ");
    mx_printinterr(linenum);
    mx_printerr(" is not valid\n");
}

static void close_fd_delete_str(int *fd, char *str) {
    close(*fd);
    mx_strdel(&str);
}

static void free_island_bridges(t_info *info, int *fd, char *str) {
    close_fd_delete_str(fd, str);
    mx_free_islands(info->islands);
    mx_free_bridges(info->bridge_list);
}

static void push(t_info *info, char *str, int linenum, bool *flag) {
    if (*flag == true) {
        mx_push_island(info, mx_get_first_island(str), flag);
        mx_push_island(info, mx_get_second_island(str), flag);
        mx_push_bridge(info, str, linenum, wrong_format);
    }
}

bool mx_split_islands(t_info *info, char *file) {
    int fd = open(file, O_RDONLY);
    char *str = NULL;
    int linenum = 2;
    bool flag = true;

    mx_read_line(&str, 1, '\n', fd);
    for (t_ll i = mx_count_enters(file) - 1; i > 0; --i, ++linenum) {
        mx_read_line(&str, 1, '\n', fd);
        if (mx_is_line_valid(str))
            push(info, str, linenum, &flag);
        else {
            wrong_format(linenum);
            free_island_bridges(info, &fd, str);
            return false;
        }
    }
    close_fd_delete_str(&fd, str);
    return true;
}
