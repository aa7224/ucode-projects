#include "pathfinder.h"

static void free_vars(t_info *info) {
    mx_free_bridges(info->bridge_list);
    mx_free_islands(info->islands);
    mx_del_cost_matrix(info->cost_matrix, info->islandsnum);
}

static void init_structs(t_info *info, int enters) {
    info->bridge_list = (t_bridge **)malloc(sizeof(t_bridge *) * (enters + 1));
    info->islands = (t_island **)malloc(sizeof(t_island *)
                  * (info->islandsnum + 1));
    for (t_ll i = 0; i < info->islandsnum + 1; ++i)
        info->islands[i] = NULL;
    for (int i = 0; i < enters + 1; ++i)
        info->bridge_list[i] = NULL;
}

static bool full_check(t_info *info, int argnum, char *arguments[]) {
    if (mx_is_valid_argc(argnum) && mx_is_valid_file(arguments[1])
     && mx_is_file_empty(arguments[1])
     && mx_is_valid_first_line(info, arguments[1]))
        return true;
    return false;
}

int main(int argc, char *argv[]) {
    t_info info;

    info.islands = NULL;
    info.bridge_list = NULL;
    info.cost_matrix = NULL;
    info.islandsnum = 0;
    info.is_f_line_valid = true;
    if (full_check(&info, argc, argv)) {
        init_structs(&info, mx_count_enters(argv[1]));
        if (mx_split_islands(&info, argv[1])) {
            if (mx_is_valid_islandsnum(&info, argv[1])) {
                mx_give_ids(&info);
                info.cost_matrix = mx_create_cost_matrix(&info);
                mx_dijkstra(&info);
                free_vars(&info);
            }
            else
                free_vars(&info);
        }
    }
}
