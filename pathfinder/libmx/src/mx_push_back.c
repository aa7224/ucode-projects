#include "../../libmx/inc/libmx.h"

void mx_push_back(t_list **list, void *data) {
    t_list *node = mx_create_node(data);

    if (*list && list && node) {
        t_list *temp = *list;

        while (temp->next)
            temp = temp->next;
        temp->next = node;
    }
    else
        *list = node;
}
