#include "libmx.h"

int mx_atoi(const char *str) {
    int sum = 0;
    int sign = 0;

    while (mx_isspace(*str))
        str++;

    if (*str == '+' || *str == '-')
        sign = *str++ == '-';

    while (*str && mx_isdigit(*str))
        sum = sum * 10 + *str++ - 48;

    return sign ? -sum : sum;
}
