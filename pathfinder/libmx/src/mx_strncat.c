#include "libmx.h"

char *mx_strncat(char *restrict s1, const char *restrict s2, int n) {
    int len1;

    if (n < 0)
        return s1;

    len1 = mx_strlen(s1);

    for (int i = 0; i < n; i++)
        s1[i + len1] = s2[i];

    s1[len1 + n] = '\0';
    return s1;
}
