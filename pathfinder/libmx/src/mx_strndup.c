#include "libmx.h"

static int min_int(int a, int b) {
    return a > b ? b : a;
}

char *mx_strndup(const char *s1, size_t n) {
    int len = mx_strlen(s1);
    char *newstr =  (s1 ? mx_strnew(min_int(len, n)) : NULL);

    return newstr ? mx_strncpy(newstr, s1, min_int(len, n)) : NULL;
}
