#include "libmx.h"

char *mx_strjoin(const char *s1, const char *s2) {
    int s1len;
    int s2len;
    char *joinedstr;
    
    if (s1 && s2) {
        s1len = mx_strlen(s1);
        s2len = mx_strlen(s2);
        joinedstr = mx_strnew(s1len + s2len);

        mx_strcpy(joinedstr, s1);
        mx_strcpy(joinedstr + s1len, s2);
        return joinedstr;
    }
    else if (s1)
        return (char *)s1;
    else if (s2)
        return (char *)s2;
    else
        return NULL;
}
