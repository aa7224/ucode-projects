#include "libmx.h"

void mx_clear_list(t_list **head) {
    if (head && *head) {
        while (*head)
            mx_pop_front(head);
    }
}
