#include "libmx.h"

static int mx_strlen_to_delim(const char *c, const char delim) {
    int i = 0;
    
    while (*c && *c != delim) {
        i++;
        c++;
    }
    return i;
}

char **mx_strsplit(const char *s, char c) {
    int i = 0;
    char **str = NULL;

    if (s && c && mx_strlen(s)) {
        bool flag = false;
        str = (char **)malloc(sizeof(char *) * mx_count_words(s, c));
        for (; *s; s++) {
            if (*s == c) {
                flag = false;
            }
            else if (flag == false) {
                str[i] = mx_strnew(mx_strlen_to_delim(s, c));
                mx_strncpy(str[i], s, mx_strlen_to_delim(s, c));
                flag = true;
                i++;
            }
        }
        return str;
    }
    return NULL;
}
