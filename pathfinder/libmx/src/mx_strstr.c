#include "libmx.h"

char *mx_strstr(const char *haystack, const char *needle) {
    int needlen;

    if (!haystack || !needle)
        return NULL;

    needlen = mx_strlen(needle ? needle : "");

    if (needlen == 0)
        return (char *)haystack;
    while (*haystack) {
        if (mx_strncmp(haystack, needle, needlen) == 0)
            return (char *)haystack;
        haystack++;
    }
    return NULL;
}
