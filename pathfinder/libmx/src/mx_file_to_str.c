#include "libmx.h"

static int size_of_file(const char *file) {
    int size = 0;
    int sz = 0;
    int fd = 0;
    char buffer[256];

    if ((fd = open(file, O_RDONLY)) == -1)
        return 0;
    
    while ((sz = read(fd, buffer, 256)))
        size += sz;
    close(fd);
    return size;
}

char *mx_file_to_str(const char *file) {
    int fd = 0;
    int size = 0;
    char *str = NULL;

    if ((size = size_of_file(file)) == 0)
        return NULL;
    fd = open(file, O_RDONLY);
    str = mx_strnew(size);
    read(fd, str, size);
    close(fd);
    return str;
}
