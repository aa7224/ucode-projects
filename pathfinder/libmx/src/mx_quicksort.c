#include "libmx.h"

static void swap(char **str1, char **str2) {
	char *temp = *str1;
	*str1 = *str2;
	*str2 = temp;
}

static int partition (char **arr, int left, int right, int *counter) {
    char *pivot = arr[left + (right - left) / 2];
    int i = left - 1;
  
    for (int j = left; j <= right; j++) {
        if (mx_strlen(arr[j]) < mx_strlen(pivot)) {
            i++;
            swap(&arr[i], &arr[j]); 
			(*counter)++;
        } 
    }
    swap(&arr[i + 1], &arr[right]); 
    return i + 1; 
}

static int sort(char **arr, int left, int right) {
	int count = 0;

    if (left < right) 
    { 
        int pi = partition(arr, left, right, &count); 

        count += sort(arr, left, pi - 1); 
		count += sort(arr, pi + 1, right); 
    } 
	return count;
} 

int mx_quicksort(char **arr, int left, int right) {
	return arr ? sort(arr, left, right) - 1 : -1;
}
