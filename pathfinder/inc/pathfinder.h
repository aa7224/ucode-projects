#ifndef PATHFINDER_H
#define PATHFINDER_H

#include "libmx.h"

#define BARRIER "========================================\n"
#define INF 2147483647

typedef long long t_ll;

typedef struct s_bridge {
    int id_from;
    int id_to;
    char *first;
    char *second;
    int distance;
} t_bridge;

typedef struct s_island {
    int id;
    char *name;
} t_island;

typedef struct s_info {
    t_island **islands;
    t_bridge **bridge_list;
    t_list *routes;

    t_ll islandsnum;
    int **cost_matrix;
    bool is_f_line_valid;
} t_info;

typedef struct s_dij {
    t_list **parent;
    t_list *routes;
    int *dist;
    int src;
    bool *visited;

    t_ll islandsnum;
} t_dij;

void mx_dijkstra(t_info *info);
void mx_create_routes(t_dij *t);
void mx_print_routes(t_dij *t, t_info *info);

//                  SPLIT ISLAND
bool mx_is_line_valid(char *line);
bool mx_split_islands(t_info *info, char *file);
void mx_push_island(t_info *info, char *island, bool *flag);
void mx_push_bridge(t_info *info, char *line, int linenum,void (error)(int));

//                  ERROR HANDLING
bool mx_is_valid_first_line(t_info *info, char *file);
bool mx_is_valid_argc(int argc);
bool mx_is_valid_file(char *file);
bool mx_is_file_empty(char *file);
int mx_is_valid_islandsnum(t_info *info, char *file);

//                  DIJKSTRA UTIL
int mx_min_distance(t_dij *t);

//                  NODE WORK
t_island *mx_create_island(int id, char *name);
void mx_give_ids(t_info *info);
int **mx_create_cost_matrix(t_info *info);

//                  GETTERS
char *mx_get_first_island(char *str);
char *mx_get_second_island(char *str);
int mx_get_distance(char *str);
void *mx_get_back_list(t_list *list);
t_list *mx_get_element_by_index(t_list *list, int index);
t_list *mx_list_cpy(t_list *src);

//                  FREE MEMORY
void mx_del_cost_matrix(int **cost_matrix, int size);
void mx_free_bridges(t_bridge **bridges);
void mx_free_islands(t_island **islands);
void mx_free_dijksrta_var(t_dij *t, t_info *info);

//                  Helpers
int mx_count_enters(char *line);
void mx_printinterr(int n);
int *mx_return_int(int num);

#endif
