#include "libmx.h"

static void num_to_chararr(long num, char *str, int *size) {
    if(num > 9)
        num_to_chararr(num / 10, str, size);
    str[(*size)++] = num % 10 + 48;
}

char *mx_itoa(int number) {
    int len = 0;
    long lnumber = number;
    char *str = NULL;

    len = (lnumber < 0) ? mx_num_strlen(-lnumber) : mx_num_strlen(lnumber);
    str = (lnumber < 0) ? mx_strnew(len + 1) : mx_strnew(len);

    if (number < 0) {
        lnumber = -lnumber;
        str[0] = '-';
        len = 1;
    }
    else
        len = 0;
    
    num_to_chararr(lnumber, str, &len);
    return str;
}
