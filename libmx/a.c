#include "inc/libmx.h"
#include <malloc/malloc.h>
#include <stdio.h>

// int sort(int *arr, int left, int right) {
//     int i = left;
//     int j = right;
//     int shifts = 0;
//     int x = arr[left + (right - left) / 2];

//     while(i <= j) {
//         for (; arr[i] < x; ++i);
//         for (; arr[j] > x; --j);

//         if (i <= j) {
//             if(arr[j] < arr[i]) {
//                 mx_swap_int(arr + i, arr + j);
//                 ++shifts;
//             }
//             ++i;
//             --j;
//         }
//     }
//     return shifts += (i < right ? sort(arr, i, right) : 0) + (left < j ? sort(arr, left, j) : 0);
// }

// void print(int *arr, int size) {
//     for (int i = 0; i < size; ++i)
//         printf("%i, ", arr[i]);
//     printf("\n");
// }

int main() {

    // Errors
    // mx_memrchr
    //printf("%s\n",mx_memrchr("Trinity", 'i', 7) );
    //Done
    // mx_strncpy
    // mx_strsplit
    // printf("%s\n", mx_strtrim(NULL)); // empty string with \0
    // printf("%s\n",mx_del_extra_spaces(NULL));
    // printf("%s\n", mx_file_to_str(NULL)); // (must return NULL)
    // printf("%s\n", mx_replace_substr("mam", "mam", "fgaaaaaaaaaa"));
    // printf("%s\n", mx_strjoin("Hello ", NULL));

    // int arr[] = {1, 6, 3, 5, 9, 8, 1, 6, 7};
    // int arr[] = {4, 3, 7, 4, 6, 2, 4, 1, 1, 1, 6, 6, 7, 0, 23, 76, 5, 5, 4, 4, 9};
    // int arr[] =  {13, 10, 8, 7, 13, 10, 8, 7};
    // int arr[] = {5, 4, 6, -2, 0, 10, 9};
    // int arr[] =  {4, 3, 7, 4, 6, 2, 4, 1, 9};

    // printf("%i\n", sort(arr, 0, 8));
    // print(arr, 9);

    // <==============================================================================> //

    // printf("%s: %i\n", "0", mx_atoi("0"));
    // printf("%s: %i\n", "1", mx_atoi("1"));
    // printf("%s: %i\n", "-1", mx_atoi("-1"));
    // printf("%s: %i\n", "100000000", mx_atoi("100000000"));
    // printf("%s: %i\n", "-10000000", mx_atoi("-10000000"));
    // printf("%s: %i\n", "-2147483648", mx_atoi("-2147483648"));
    // printf("%s: %i\n", "2147483647", mx_atoi("2147483647"));
    // printf("%s: %i\n", "1234567890", mx_atoi("1234567890"));

    // <==============================================================================> //

    // char *arr[] = {"222", "Abcd", "aBc", "ab", "az", "z"};
    // int count;

    // printf("%i, %i\n", mx_binary_search(arr, 6, "ab", &count), count);
    // printf("%i, %i\n", mx_binary_search(arr, 6, "aBc", &count), count);

    // <==============================================================================> //

    // char *arr1[] = {"abc", "xyz", "ghi", "def"};
    // char *arr2[] = {"abc", "acb", "a"};

    // printf("%i\n", mx_bubble_sort(arr1, 4));
    // printf("%i\n", mx_bubble_sort(arr2, 3));

    // <==============================================================================> //

    // void mx_clear_list(t_list * *list);

    // <==============================================================================> //

    // char *str[] = {"yo, yo, yo Neo", "aa aa aa"};
    // char *sub[] = {"yo", "aa"};

    // printf("%i = 3\n", mx_count_substr(str[0], sub[0]));
    // printf("%i = -1\n", mx_count_substr(str[0], NULL));
    // printf("%i = -1\n", mx_count_substr(NULL, sub[0]));

    // printf("%i = 3\n", mx_count_substr(str[1], sub[1]));

    // <==============================================================================> //

    // char str[] = " follow * the white rabbit ";
    // printf("%i = 2\n", mx_count_words(str, '*'));
    // printf("%i = 5\n", mx_count_words(str, ' '));
    // printf("%i = -1\n", mx_count_words(NULL, ' '));

    // <==============================================================================> //

    // t_list *mx_create_node(void *data);

    // <==============================================================================> //

    // char *str[] = {"\f My name... is \r Neo \t\n ", "", " ", NULL};

    // for (unsigned long i = 0; i < sizeof(str) / sizeof(str[0]); ++i)
    //     printf(":%s:\n", mx_del_extra_spaces(str[i]));

    // <==============================================================================> //

    // void mx_del_node_if(t_list * *list, void *del_data, bool (*cmp)(void *a, void *b));

    // <==============================================================================> //

    // char **arr = (char **)malloc(sizeof(char *) * 5);
    // printf("%zu\n", malloc_size(arr));

    // for (int i = 0; i < 4; ++i) {
    //     arr[i] = (char *)malloc(sizeof(char) * 5);
    //     printf("%zu\n", malloc_size(arr[i]));
    // }

    // arr[4] = NULL;
    // mx_del_strarr(&arr);
    // printf("\n%zu\n", malloc_size(arr));

    // <==============================================================================> //

    // unsigned long long mx_factorial(unsigned long long n);

    // <==============================================================================> //

    // char *names[] = {"txt.txt", "./txt.txt", "txt", "qwerty", NULL};
    // char *res = NULL;

    // for (unsigned long i = 0; i < sizeof(names) / sizeof(names[0]); ++i) {
    //     res = mx_file_to_str(names[i]);
    //     printf(":%s: %zu\n", res, malloc_size(res));
    // }

    // <==============================================================================> //

    // int arr[] = {1, 2, 3, 4, 5};

    // mx_foreach(arr, 5, print);
    // mx_foreach(arr, 5, NULL);
    // mx_foreach(NULL, 5, print);

    // <==============================================================================> //

    // void mx_foreach_list(t_list * list, void (*f)(t_list * node));

    // <==============================================================================> //

    // char c[] = {' ', 'c', 'z', '\n', '\0', '\0'};
    // char *str[] = {"", "abcdef", "zzzz", "\n\n\0", "\n\n\0", "\n\n"};

    // for (unsigned long i = 0; i < sizeof(c) / sizeof(c[0]); ++i)
    //     printf("%lu - :%s:\t:%c:\t:%i:\n", i, str[i], c[i], mx_get_char_index(str[i], c[i]));

    // <==============================================================================> //

    // int mx_get_length(long long num, unsigned base);

    // <==============================================================================> //

    // char *haystack[] = {"McDonalds", "McDonalds Donuts", "McDonalds", "McDonalds", NULL};
    // char *needle[] = {"Don", "on", "Donatello", NULL, "Don"};

    // for (unsigned long i = 0; i < sizeof(haystack) / sizeof(haystack[0]); ++i)
    //     printf(":%s: :%s: :%i:\n", haystack[i], needle[i], mx_get_substr_index(haystack[i], needle[i]));

    // <==============================================================================> //

    // printf("%s: %lu\n", "C4", mx_hex_to_nbr("C4"));
    // printf("%s: %lu\n", "FADE", mx_hex_to_nbr("FADE"));
    // printf("%s: %lu\n", "ffffffffffff", mx_hex_to_nbr("ffffffffffff"));
    // printf("%s: %lu\n", "FFFFFFFFFFFF", mx_hex_to_nbr("FFFFFFFFFFFF"));
    // printf("%s: %lu\n", NULL, mx_hex_to_nbr(NULL));
    // printf("%s: %lu\n", "ABCDEFABCDEF", mx_hex_to_nbr("ABCDEFABCDEF"));
    // printf("%s: %lu\n", "abcdefabcdef", mx_hex_to_nbr("abcdefabcdef"));

    // <==============================================================================> //

    // bool mx_isalpha(int c);

    // <==============================================================================> //

    // bool mx_isdigit(int c);

    // <==============================================================================> //

    // bool mx_ishex(int c);

    // <==============================================================================> //

    // bool mx_islower(int c);

    // <==============================================================================> //

    // bool mx_isodd(long long value);

    // <==============================================================================> //

    // bool mx_isprime(unsigned long long num);

    // <==============================================================================> //

    // bool mx_isspace(int c);

    // <==============================================================================> //

    // bool mx_isupper(int c);

    // <==============================================================================> //

    // for (int i = -10; i < 11; ++i)
    //     printf("%i: %s\n", i, mx_itoa(i));

    // printf("%i: %s\n", 2147483647, mx_itoa(2147483647));
    // printf("%li: %s\n", -2147483648, mx_itoa(-2147483648));

    // <==============================================================================> //

    // int mx_list_size(t_list * list);

    // <==============================================================================> //

    // void *mx_memccpy(void *restrict dst, const void *restrict src, int c, size_t n);

    // <==============================================================================> //

    // void *mx_memchr(const void *s, int c, size_t n);

    // <==============================================================================> //

    // int mx_memcmp(const void *s1, const void *s2, size_t n);

    // <==============================================================================> //

    // void *mx_memcpy(void *restrict dst, const void *restrict src, size_t n)

    // <==============================================================================> //

    // char *s1[] = {"abab", "",  " "};
    // char *s2[] = {"ba",   " ", ""};

    // for (unsigned long i = 0; i < sizeof(s1) / sizeof(s1[0]); ++i)
    //     printf(":%s:\n", mx_memmem(s1, 1, s2, 1));

    // <==============================================================================> //

    // void *mx_memmove(void *dst, const void *src, size_t len);

    // <==============================================================================> //

    // void *mx_memrchr(const void *s, int c, size_t n);

    // <==============================================================================> //

    // void *mx_memset(void *b, int c, size_t len);

    // <==============================================================================> //

    // printf("%i: %s\n", 0, mx_nbr_to_hex(0));
    // printf("%i: %s\n", 1, mx_nbr_to_hex(1));
    // printf("%i: %s\n", 11, mx_nbr_to_hex(11));
    // printf("%i: %s\n", 52, mx_nbr_to_hex(52));
    // printf("%i: %s\n", 777, mx_nbr_to_hex(777));
    // printf("%i: %s\n", 1000, mx_nbr_to_hex(1000));
    // printf("%i: %s\n", 9999, mx_nbr_to_hex(9999));
    // printf("%i: %s\n", 1010101, mx_nbr_to_hex(1010101));

    // <==============================================================================> //

    // void mx_pop_back(t_list * *list);

    // <==============================================================================> //

    // void mx_pop_front(t_list * *list);

    // <==============================================================================> //

    // void mx_pop_index(t_list * *list, int index);

    // <==============================================================================> //

    // printf("%i %u: %lf\n", 5, 4, mx_pow(5, 4));
    // printf("%i %u: %lf\n", 5, 1, mx_pow(5, 1));
    // printf("%i %u: %lf\n", 1, 5, mx_pow(1, 5));
    // printf("%i %u: %lf\n", 4, 5, mx_pow(4, 5));
    // printf("%i %u: %lf\n", 1, 0, mx_pow(1, 0));
    // printf("%i %u: %lf\n", 0, 1, mx_pow(0, 1));
    // printf("%i %u: %lf\n", -1, 0, mx_pow(-1, 0));
    // printf("%i %u: %lf\n", -2, 2, mx_pow(-2, 2));
    // printf("%lf %u: %lf\n", 1.5, 4, mx_pow(1.5, 4));
    // printf("%lf %u: %lf\n", -0.1, 3, mx_pow(-0.1, 3));

    // <==============================================================================> //

    // char *arr1[] = {"a", "b", "c", NULL};
    // char *arr2[] = {NULL};

    // mx_print_strarr(arr1, "\n");
    // mx_print_strarr(arr2, "\n");

    // <==============================================================================> //

    // mx_print_unicode(L'\u0444');
    // mx_print_unicode(L'ф');
    // mx_print_unicode(L'ы');
    // mx_print_unicode(L'ъ');
    // mx_print_unicode(L'ё');
    // mx_print_unicode(L'і');
    // mx_print_unicode(L'ї');
    // mx_print_unicode(L'й');
    // printf("\n");

    // <==============================================================================> //

    // for (int i = 32; i < 127; ++i)
    //     mx_printchar(i);
    // printf("\n");

    // <==============================================================================> //

    // mx_printint(-2147483648);
    // printf("\n");
    // mx_printint(2147483647);
    // printf("\n");
    // mx_printint(10000);
    // printf("\n");
    // mx_printint(-10000);
    // printf("\n");
    // mx_printint(0);
    // printf("\n");
    // mx_printint(1234567890);
    // printf("\n");
    // mx_printint(-1234567890);
    // printf("\n");

    // <==============================================================================> //

    // mx_printstr("нашальникэ\n");
    // mx_printstr("mijnhbgyvtfcrdrftvgybhunjimko,ojinhubgyvf\n");
    // mx_printstr("");
    // mx_printstr("\n");

    // <==============================================================================> //

    // void mx_push_back(t_list * *list, void *data);

    // <==============================================================================> //

    // void mx_push_front(t_list * *list, void *data);

    // <==============================================================================> //

    // void mx_push_index(t_list * *list, void *data, int index);

    // <==============================================================================> //

    // char *rstr[] = {mx_strnew(15), mx_strnew(15), mx_strnew(15)};

    // printf("1: %zu %zu %zu\n", malloc_size(rstr[0]), malloc_size(rstr[1]), malloc_size(rstr[2]));

    // for (int i = 0; i < 3; ++i)
    //     scanf("%s", rstr[i]);

    // char *nstr[] = {realloc(rstr[0], 20), realloc(rstr[1], 0), realloc(rstr[2], 10)};

    // for (int i = 0; i < 3; ++i)
    //     printf("r: %s\nn: %s\n", rstr[i], nstr[i]);
    // printf("\n");

    // printf("2: %zu %zu %zu %p %p %p\n",
    //     malloc_size(rstr[0]), malloc_size(rstr[1]), malloc_size(rstr[2]),
    //     (void *)rstr[0], (void *)rstr[1], (void *)rstr[2]);

    // printf("3: %zu %zu %zu %p %p %p\n",
    //     malloc_size(nstr[0]), malloc_size(nstr[1]), malloc_size(nstr[2]),
    //     (void *)nstr[0], (void *)nstr[1], (void *)nstr[2]);

    // char *rstr2[] = {mx_strnew(15), mx_strnew(15), mx_strnew(15)};

    // printf("4: %zu %zu %zu\n", malloc_size(rstr2[0]), malloc_size(rstr2[1]), malloc_size(rstr2[2]));

    // char *nstr2[] = {mx_realloc(rstr2[0], 20), mx_realloc(rstr2[1], 0), mx_realloc(rstr2[2], 10)};

    // printf("5: %zu %zu %zu %p %p %p\n",
    //     malloc_size(rstr2[0]), malloc_size(rstr2[1]), malloc_size(rstr2[2]),
    //     (void *)rstr2[0], (void *)rstr2[1], (void *)rstr2[2]);

    // printf("6: %zu %zu %zu %p %p %p\n",
    //     malloc_size(nstr2[0]), malloc_size(nstr2[1]), malloc_size(nstr2[2]),
    //     (void *)nstr2[0], (void *)nstr2[1], (void *)nstr2[2]);

    // <==============================================================================> //

    // char *str[] = {"McDonalds", "Ururu turu", "abababababa", "",  "a", "a", "aa", "aaaaa"};
    // char *sub[] = {"alds",      "ru",         "aba",         "a", "",  "a", "",   "aaa"};
    // char *rep[] = {"uts",       "ta",         "aa",          "b", "b", "",  "",   ""};

    // for (unsigned long i = 0; i < sizeof(str) / sizeof(str[0]); ++i)
    //     printf("%lu :%s:\n", i, mx_replace_substr(str[i], sub[i], rep[i]));

    // <==============================================================================> //

    // int pos = 1;
    // int fd = open("txt.txt", 0);
    // char *str = NULL;

    // while (pos) {
    //     pos = mx_read_line(&str, 1, 't', fd);
    //     printf("%s\n", str);
    // }

    // close(fd);

    // <==============================================================================> //

    // char *arr1[] = {"666666", "333", "999999999", "1", "88888888", "4444", "55555", "7777777", "22"};
    // char *arr2[] = {"Michelangelo", "Donatello", "Leonardo", "Raphael"};

    // printf("%i\n", mx_quicksort(arr1, 0, 8));
    // printf("%i\n", mx_quicksort(arr2, 0, 3));

    // <==============================================================================> //

    // t_list *mx_sort_list(t_list * list, bool (*cmp)(void *a, void *b));

    // <==============================================================================> //

    // for (int i = -2147483648; i < 2147483647; ++i)
    //     if (mx_sqrt(i) != i_sqrt(i))
    //         printf("%i %i %i\n", i, mx_sqrt(i), i_sqrt(i));

    // <==============================================================================> //

    // char str1[] = "game over";
    // mx_str_reverse(str1); //'str' now is "revo emag"
    // printf("%s\n", str1);

    // char str2[] = "r";
    // mx_str_reverse(str2);
    // printf("%s\n", str2);

    // char str3[] = "";
    // mx_str_reverse(str3);
    // printf("%s\n", str3);

    // mx_str_reverse(NULL);

    // <==============================================================================> //

    // char *mx_strcat(char *restrict s1, const char *restrict s2);

    // <==============================================================================> //

    // for (int i = 0; i < 20; ++i)
    //     printf("%s %s\n", "9876543210", mx_strchr("9876543210", '0' + i));

    // <==============================================================================> //

    // int mx_strcmp(const char *s1, const char *s2);

    // <==============================================================================> //

    // char src[] = "ABC";
    // char *dst = mx_strnew(3);

    // mx_strcpy(dst, src);
    // printf("%s\n", dst);

    // <==============================================================================> //

    // char *str = mx_strnew(22);
    // printf("%zu\n", malloc_size(str));
    // mx_strdel(&str);
    // printf("%zu\n", malloc_size(str));

    // <==============================================================================> //

    // char *str[] = {"ABC", "A", "", "\0"};
    // char *cpy[] = {NULL, NULL, NULL, NULL};

    // for (unsigned long i = 0; i < sizeof(str) / sizeof(str[0]); ++i) {
    //     cpy[i] = mx_strdup(str[i]);
    //     printf(":%s: %zu\n", cpy[i], malloc_size(cpy[i]));
    // }

    // <==============================================================================> //

    // char *str[] = {"aaa", "bbb", "", "ccc", NULL, ""};
    // char *res = NULL;

    // for (unsigned long i = 0; i < sizeof(str) / sizeof(str[0]) - 1; ++i) {
    //     res = mx_strjoin(str[i], str[i + 1]);
    //     printf(":%s: %zu\n", res, malloc_size(res));
    // }

    // <==============================================================================> //

    // printf(":%s: %i\n", "", mx_strlen(""));
    // printf(":%s: %i\n", "a", mx_strlen("a"));
    // printf(":%s: %i\n", "aa", mx_strlen("aa"));
    // printf(":%s: %i\n", "\\\\\\\\", mx_strlen("\\\\\\\\"));
    // // printf(":%s: %i\n", NULL, mx_strlen(NULL));

    // <==============================================================================> //

    // int mx_strncmp(const char *s1, const char *s2, int n);

    // <==============================================================================> //

    // char *src[] = {"abc", "a", "7777777"};
    // char *dst[] = {mx_strnew(3), mx_strnew(1), mx_strnew(7)};
    // // char *dst[] = {mx_strnew(11), mx_strnew(11), mx_strnew(11)};
    // int sz[] = {3, 1, 7};
    // // int arr[] = {1, 1, 1};
    // int arr[] = {1, 1, 1};

    // for (unsigned long i = 0; i < sizeof(src) / sizeof(src[0]); ++i) {
    //     mx_strncpy(dst[i], src[i], arr[i]);
    //     printf("%s %zu\n", dst[i], malloc_size(dst[i]));

    //     for (int j = 0; j <= sz[i]; ++j)
    //         printf("%i, ", dst[i][j]);
    //     printf("\n");
    // }

    // <==============================================================================> //

    // char *str[] = {"ABC", "A", "", "\0"};
    // char *cpy[] = {NULL, NULL, NULL, NULL};

    // for (unsigned long i = 0; i < sizeof(str) / sizeof(str[0]); ++i) {
    //     cpy[i] = mx_strndup(str[i], 1); // 1, 100
    //     printf(":%s", cpy[i]);
    // }

    // <==============================================================================> //

    // char *mx_strnew(const int size);

    // <==============================================================================> //

    // char *str[] = {"**Good bye,**Mr.*Anderson.****", " Knock, knock, Neo. ", "", NULL};
    // char del[] = {'*', ' ', ' ', ' '};

    // for (unsigned long j = 0; j < sizeof(str) / sizeof(str[0]); ++j) {
    //     char **arr = mx_strsplit(str[j], del[j]);

    //     for (int i = 0; arr && arr[i]; ++i)
    //         printf(":%s:\n", arr[i]);

    //     printf("\n");
    // }

    // <==============================================================================> //

    // char *haystack[] = {"McDonalds", "McDonalds Donuts", "McDonalds", "McDonalds", NULL};
    // char *needle[] = {"Don", "on", "Donatello", NULL, "Don"};

    // for (unsigned long i = 0; i < sizeof(haystack) / sizeof(haystack[0]); ++i)
    //     printf(":%s: :%s: :%s:\n", haystack[i], needle[i], mx_strstr(haystack[i], needle[i]));

    // <==============================================================================> //

    // char *str[] = {"\f My name... is Neo \t\n ", "    ttt", "ttt    ", "\r\n\t\f\v ", "", NULL};

    // for (unsigned long i = 0; i < sizeof(str) / sizeof(str[0]); ++i)
    //     printf(":%s:\n", mx_strtrim(str[i]));

    // <==============================================================================> //

    // int mx_sum_digits(int num);

    // <==============================================================================> //

    // char str[] = "ONE";
    // printf("%s\n", str);

    // mx_swap_char(&str[0], &str[1]); //'str' now is "NOE"
    // printf("%s\n", str);

    // mx_swap_char(&str[1], &str[2]); //'str' now is "NEO"
    // printf("%s\n", str);

    // mx_swap_char(&str[1], NULL);
    // printf("%s\n", str);

    // mx_swap_char(NULL, &str[1]);
    // printf("%s\n", str);

    // mx_swap_char(NULL, NULL);
    // printf("%s\n", str);

    // <==============================================================================> //

    // void mx_swap_int(int *i1, int *i2);

    // <==============================================================================> //

    // void mx_swap_str(char **s1, char **s2);

    // <==============================================================================> //

    // int mx_tolower(int c);

    // <==============================================================================> //

    // int mx_toupper(int c);
}
