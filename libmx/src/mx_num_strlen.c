unsigned mx_num_strlen(unsigned number) {
    unsigned i = 0;

    while (number != 0) {
        i++;
        number /= 10;
    }
    return i;
}
