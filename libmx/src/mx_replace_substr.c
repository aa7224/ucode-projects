#include "libmx.h"

char *mx_replace_substr(const char *str, const char *sub, const char *replace) {
    if (str && sub && replace) {
        int substr_count = mx_count_substr(str, sub);
        int sublen = mx_strlen(sub);
        int replacelen = mx_strlen(replace);
        int i = 0;
        char *newstr = NULL;

        if (sublen == 0 || replacelen == 0)
            return mx_strdup(str);

        newstr = mx_strnew(mx_strlen(str) - sublen * substr_count
                                 + replacelen * substr_count);
        
        while (*str) {
            if (mx_strncmp(str, sub, sublen) == 0) {
                mx_strcat(newstr, replace);
                str += sublen;
                i += replacelen;
            }
            else {
                newstr[i++] = *str;
                str++;
            }
        }
        return newstr;
    }
    return NULL;    
}
