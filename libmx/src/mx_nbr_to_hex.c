#include "libmx.h"

static int size_of_hex(unsigned long nbr) {
    int size = 0;
    int temp = 0;

    
    while (nbr != 0) {
        temp = 0;
        temp = nbr % 16;

        if (temp < 10)
            size++;
        else
            size++;
        nbr = nbr / 16;
    }
    return size;
}

char *mx_nbr_to_hex(unsigned long nbr) {
    char *hex = mx_strnew(size_of_hex(nbr));
    int i = size_of_hex(nbr) - 1;
    int temp = 0;

    if (nbr == 0)
        hex[0] = '0';
    while (nbr != 0) {
        temp = 0;
        temp = nbr % 16;

        if (temp < 10)
            hex[i--] = temp + 48;
        else
            hex[i--] = temp + 87;
        nbr = nbr / 16;
    }
    return hex;
}
