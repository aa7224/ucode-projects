#include "libmx.h"

static int size_without_spaces(const char *str) {
    int size = 0;

    while (*str) {
        while (mx_isspace(*str) && mx_isspace(*(str + 1)))
            str++;
        size++;
        str++;
    }
    return size;
}

char *mx_del_extra_spaces(const char *str) {
    char *dest;
    char *str_trimmed;
    int i = 0;
    int j = 0;

    if (!str)
        return NULL;

    str_trimmed = mx_strtrim(str);
    dest = mx_strnew(size_without_spaces(str_trimmed));

    while (str_trimmed[j]) {
        while (mx_isspace(str_trimmed[j]) && mx_isspace(str_trimmed[j + 1]))
            j++;
        dest[i++] = (mx_isspace(str_trimmed[j]) ? ' ' : str_trimmed[j]);
        j++;
    }
    mx_strdel(&str_trimmed);
    return dest;
}
