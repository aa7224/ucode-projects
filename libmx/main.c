#include "inc/libmx.h"
#include <stdio.h>
bool cmp(void *data1, void *data2) {
    return data1 > data2;
}

int main() {

    //                                          QUICK SORT CHECK
    // char *str[5] = {"Michelangelo", "Donatello", "Leonardo", "Raphael", NULL};

    // printf("%d\n", mx_quicksort(str, 0, 3));
    // mx_print_strarr(str, "\n");

    //                                          COUNT SUBSTR CHECK
    // char *str = "yo, yo, yo Neo";
    // char *sub = "yo";

    // printf("%d\n", mx_count_substr(str, sub));
    //printf("%d\n", mx_count_substr(str, NULL));
    //printf("%d\n", mx_count_substr(NULL, sub));
    //                                          COUNT WORDS CHECK
    // char *str = " follow * the white rabbit ";

    // printf("%d\n", mx_count_words(str, '*'));
    // printf("%d\n", mx_count_words(str, ' '));
    // printf("%d\n", mx_count_words(NULL, ' '));
    //                                          DEL EXTRA SPACES CHECK
    // char *name = "\f My name... is                           \r Neo \t\nWORDS";

    // name = mx_del_extra_spaces(name);
    // printf("%s\n", name);
    //                                          FILE TO STR CHECK
	// char *str = NULL;
    // str = mx_file_to_str("file");
    // printf("%s", str);
    //                                          GET CHAR INDEX CHECK
    // printf("%d", mx_get_char_index("Privet", 'i'));
    // system("leaks main");
    //                                          GET SUBSTR INDEX
    // printf("%d\n", mx_get_substr_index("McDonalds", "Don"));       //returns 2
    // printf("%d\n", mx_get_substr_index("McDonalds Donuts", "on")); //returns 3
    // printf("%d\n", mx_get_substr_index("McDonalds", "Donatello")); //returns -1
    // printf("%d\n", mx_get_substr_index("McDonalds", NULL));        //returns -2
    // printf("%d\n", mx_get_substr_index(NULL, "Don"));              //returns -2
    //                                          HEX TO NBR
    // printf("%lu\n", mx_hex_to_nbr("C4"));           //returns 196
    // printf("%lu\n", mx_hex_to_nbr("FADE"));         //returns 64222
    // printf("%lu\n", mx_hex_to_nbr("ffffffffffff")); //returns 281474976710655
    // printf("%lu\n", mx_hex_to_nbr(NULL));           //returns 0
    //                                          ITOA
    // char *num = mx_itoa(100);
    // printf("%s\n", num);
    // printf("%s\n", mx_itoa(0));
    // printf("%s\n", mx_itoa(-100));
    // printf("%s\n", mx_itoa(2147483647));
    // printf("%s\n", mx_itoa(-2147483648));
    //                                          NBR TO HEX
    // char *hex = mx_nbr_to_hex(52);
    // printf("%s\n", hex); //returns "34"
    //printf("%s\n", mx_nbr_to_hex(1000)); //returns "3e8"
    //                                          UNICODE ???????????????
    // wchar_t star = 0x2605;
    // mx_print_unicode(star);
    //                                          REPLACE SUBSTR
    // char *str = mx_replace_substr("McDonalds", "", "");
    // printf("\n%s\n", str); //returns "McDonuts"
    // printf("\n%s\n", mx_replace_substr("Ururu turu", "ru", "ta"));   //returns "Utata tuta"
    //                                          STR SPLIT
    // char *s = "**Good bye,**Mr.*Anderson.****";
    // char **arr = mx_strsplit(s, '*'); // arr = ["Good bye,", "Mr.", "Anderson."]
    // char *s1 = " Knock, knock, Neo. alo";
    // char **arr1 = mx_strsplit(s1, ' '); // arr = ["Knock,", "knock,", "Neo."]

    // printf("\n");
    // mx_print_strarr(arr1, "\n");
    //                                            BINARY SEARCH
    // char *arr[] = {"222", "Abcd", "aBc", "ab", "az", "z"};
    // int count = 0;
                                      
    // printf("%d %d\n", mx_binary_search(arr, 6, "ab", &count), count);
    // count = 0;
    // printf("%d %d\n", mx_binary_search(arr, 6, "aBc", &count), count);
    //                                           BUBBLE SORT
    // char *arr[] = {"abc", "xyz", "ghi", "def"};
    // printf("%d\n", mx_bubble_sort(arr, 4)); //returns 3
    // char *arr1[] = {"abc", "acb", "a"};
    // printf("%d\n", mx_bubble_sort(arr1, 3)); //returns 2
    //                                           FOREACH
    // int arr[4] = {1, 2, 3, 4};
    // mx_foreach(arr, 4, mx_printint);
    //                                            POW
    // printf("%f\n", mx_pow(0, 4));
    // printf("%f\n", mx_pow(5, 0));
    // printf("%f\n", mx_pow(0, 0));
    //                                           PRINT STR
    // mx_printstr("Alo");
    // mx_printstr("");
    // mx_printstr(NULL);
    //                                           PRINT INT
    // mx_printint(0);
    // mx_printint(2147483647);
    // mx_printint(-2147483648);
    // mx_printint(100);
    // mx_printint(-100);
    //                                          SQRT
    // printf("%d\n", mx_sqrt(3)); //returns 0
    // printf("%d\n", mx_sqrt(4)); //returns 2
    //                                          STRSTR
    //char *str = mx_strstr("Privet", "ivet");
    //printf("%s\n", str);
    // printf("%s\n", mx_strstr("Privet", ""));
    // printf("%s\n", mx_strstr("Privet", NULL));
    //                                          STRTRIM
    // char *name = "\f My name... is Neo \t\n ";
    // name = mx_strtrim(name);
    // printf(":%s:\n", name); //returns "My name... is Neo"
    //                                          LIST CHECK !!!!!!!
    int *start = (int *)malloc(sizeof(int));
    *start = 1;
    t_list *new_lst = mx_create_node(start);
    int *x = (int *)malloc(sizeof(int));
    int *x1 = (int *)malloc(sizeof(int));
    int *x2 = (int *)malloc(sizeof(int));

    *x = 2;
    *x1 = 3;
    *x2 = 4;

    printf("LSTsize: %d\n", mx_list_size(new_lst));
    mx_push_front(&new_lst, x);
    printf("LSTsize: %d\n", mx_list_size(new_lst));
    printf("dataFront: %d\n", (int)new_lst->data);
    mx_push_back(&new_lst, x2);
    printf("LSTsize: %d\n", mx_list_size(new_lst));
    printf("dataBack: %d\n", (int)new_lst->next->next->data);
    mx_pop_front(&new_lst);
    printf("LSTsize: %d\n", mx_list_size(new_lst));
    mx_pop_back(&new_lst);
    printf("LSTsize: %d\n", mx_list_size(new_lst));
    mx_sort_list(new_lst, cmp);
    printf("dataList: %d %d %d\n", (int)new_lst->data, (int)new_lst->next->data, (int)new_lst->next->next->data);
    //                                         MEMORY
    //                                         MEMSET
    // int *arr = (int *)malloc(sizeof(int) * 4);
    // for (int i = 0; i < 4; i++) {
    //     printf("%d\n", arr[i]);
    // }
    // mx_memset(arr, 0, sizeof(int) * 4);
    // for (int i = 0; i < 4; i++) {
    //     printf("%d\n", arr[i]);
    // }
    //                                      MEMCPY
    // char *str = "Privet";
    // char *dst = mx_strnew(mx_strlen(str));

    // mx_memcpy(dst, str, 3);
    // printf("%s",dst);
    //                                      MEMCCPY
    // char *str = "Privet";
    // char *dst = mx_strnew(mx_strlen(str));

    // mx_memccpy(dst, str, 'r', 7);
    // printf("%s",dst);
    //                                      MEMCMP
    // char *str = "Privet";
    // char *dst = "Prive";

    
    // printf("%d",mx_memcmp(dst, str, 7));
    //                                      MEMCHR
    // char *str = "Privet";

    // printf("%s", mx_memchr(str, 'i', 7));
    //                                      MEMRCHR
    // printf("%s\n", mx_memrchr("Trinity", 'i', 7)); //returns "ity"
    // printf("%s\n", mx_memrchr("Trinity", 'M', 7)); //returns NULL
    //                                      MEMMEM
    // printf("%s\n", mx_memmem("Privet", 7, "iv", 2));
    // printf("%s\n", mx_memmem("Privet", 7, NULL, 0));
    //                                      MEMMOVE
    // char *src = "Privet";
    // char *dst = mx_strnew(7);

    // mx_memmove(dst, src, 7);
    // printf("%s", dst);
    //                                      REALLOC ???????????
    // void *ptr1, *ptr2;
    // ptr1 = malloc(24);
    // printf("%p %lu\n", (void*)&ptr1, malloc_size(ptr1));
    // ptr2 = mx_realloc(ptr1, 6);
    // printf("%p %lu\n", (void*)&ptr2, malloc_size(ptr2));
                                        //  READ LINE
    // int fd = open("file.txt", O_RDONLY);
    // char *str = mx_strnew(200);

    // printf(":z%d:\n", mx_read_line(&str, 20, 'f', fd));
    // //printf(":z%d:\n", mx_read_line(&str, 35, 't', fd));
    // //printf(":z%d:\n", mx_read_line(&str, 4, '.', fd));
    // printf("%s", str);
	//system("leaks main");
}
